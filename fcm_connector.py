
# coding: utf-8

# In[38]:


#Connect to database
import sqlite3
conn = sqlite3.connect('fahact.sqlite3')
cur = conn.cursor()


# In[77]:


#Select data by condition from a database
cur.execute("SELECT users.firebase_token, accuracies.value, users.id, users.created_at FROM users INNER JOIN accuracies ON users.id = accuracies.user_id WHERE julianday('now') - julianday(strftime('%Y-%m-%d', users.created_at)) > 2")
results = cur.fetchall()
for result in results:
    print(result)


# In[78]:


#Submit data to Firebase Cloud Messaging 
import json
import requests

api_key = 'AAAAnUx1vLM:APA91bFfzk0KeJI-kBd9JvbqFOQ1wgF5zYnYbpeVLPcddibhTeWuY_uXxhQSM8bHWjrJr12sKaIrP1qASSUN6F5M5kYkXJ3CiIY4Dq0wN_Qh6Wwarzwr_6dH5b4ZetalIo4zbo3ZTq-0'
api_url = 'https://fcm.googleapis.com/fcm/send'
headers = {'Content-Type': 'application/json',
           'Authorization': 'key='+api_key}

if len(results) > 0:
    for result in results:
        data = {
            "to": result[0],
            "data": {
                "type": "gamify",
                "title": "Your feedback",
                "message": str(result[1])
            }
        }
        response = requests.post(api_url, headers=headers, json=data)
        print(response.text)


# In[75]:


#Select data by condition from a database
cur.execute("SELECT users.firebase_token, accuracies.value, users.id, users.created_at FROM users INNER JOIN accuracies ON users.id = accuracies.user_id WHERE julianday('now') - julianday(strftime('%Y-%m-%d', users.created_at)) <= 2")
results = cur.fetchall()
for result in results:
    print(result)


# In[80]:


#Submit data to Firebase Cloud Messaging 
import json
import requests

api_key = 'AAAAnUx1vLM:APA91bFfzk0KeJI-kBd9JvbqFOQ1wgF5zYnYbpeVLPcddibhTeWuY_uXxhQSM8bHWjrJr12sKaIrP1qASSUN6F5M5kYkXJ3CiIY4Dq0wN_Qh6Wwarzwr_6dH5b4ZetalIo4zbo3ZTq-0'
api_url = 'https://fcm.googleapis.com/fcm/send'
headers = {'Content-Type': 'application/json',
           'Authorization': 'key='+api_key}

if len(results) > 0:
    for result in results:
        data = {
            "to": result[0],
            "data": {
                "type": "naive",
                "title": "Hi",
                "message": "What are you doing?"
            }
        }
        response = requests.post(api_url, headers=headers, json=data)
        print(response.text)

